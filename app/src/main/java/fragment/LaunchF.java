package fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rsf.R;

import adapter.SliderAdapter;
import utils.AppConstant;

public class LaunchF extends BaseFragment {
    private Context context;
    private View view;
    private LinearLayout mainIndicator;
    private ViewPager launchViewpager;
    public static int[] imageRSC = { R.drawable.place_holder, R.drawable.place_holder, R.drawable.place_holder, R.drawable.place_holder};
    private String[] titleArray, desArray;
    private boolean isNetAvailable =false;

    private TextView textViewLogin,netStatus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        view = inflater.inflate(R.layout.launch, container, false);

        intUint();
        return view;
    }

    private void intUint() {

        isNetAvailable = AppConstant.isOnline(context);

        titleArray = getResources().getStringArray(R.array.titleArray);
        desArray = getResources().getStringArray(R.array.detailsArray);
        netStatus = (TextView) view.findViewById(R.id.netStatus);

        if(isNetAvailable){
            netStatus.setText("Online");

        }else{
            netStatus.setText("Offline");
        }

        mainIndicator=(LinearLayout)view.findViewById(R.id.mainIndicator);
        launchViewpager=(ViewPager)view.findViewById(R.id.launchViewpager);

        launchViewpager.setAdapter(new SliderAdapter(context, imageRSC, titleArray, desArray));
        launchViewpager.setCurrentItem(0);
        controlDots(0);
        launchViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                controlDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        textViewLogin = (TextView) view.findViewById(R.id.textViewLogin);

        textViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new LoginF(),true);
            }
        });


    }

    protected void controlDots(int newPageNumber) {
        try {
            mainIndicator.removeAllViews();
            final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                    android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(7, 2, 0, 2);
            final ImageView[] b = new ImageView[imageRSC.length];
            for (int i1 = 0; i1 < imageRSC.length; i1++) {
                b[i1] = new ImageView(context);
                b[i1].setId(1000 + i1);
                if (newPageNumber == i1) {
                    b[i1].setBackgroundResource(R.drawable.dott);
                } else {
                    b[i1].setBackgroundResource(R.drawable.dot);
                }
                b[i1].setLayoutParams(layoutParams);
                mainIndicator.addView(b[i1]);
            }
        } catch (final Exception e) {
        }
    }
}
