package fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rsf.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import adapter.CustomAdapter;
import holder.AllQuestion;
import model.DetailsObj;

public class HomeF extends Fragment {
    private Context context;
    private ProgressBar homeProgress;
    private RecyclerView my_recycler_view;
    private CustomAdapter customAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static final int numberOfColumns =2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_f,null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        intUit();
    }

    private void intUit() {

        homeProgress = (ProgressBar)getView().findViewById(R.id.homeProgress);
        my_recycler_view = (RecyclerView) getView().findViewById(R.id.my_recycler_view);
        my_recycler_view.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(context, numberOfColumns);
        my_recycler_view.setLayoutManager(layoutManager);
        my_recycler_view.setItemAnimator(new DefaultItemAnimator());


        String jsonString= loadJSONFromAsset();
        MyTask task = new MyTask();
        task.execute(jsonString);

        //getActivity().overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );

       // scheduleAlarmNotification();
    }

    public String loadJSONFromAsset() {

        String json = null;
        try {
            InputStream is = getContext().getAssets().open("question.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


    private AllQuestion parseGson(String content){

        GsonBuilder builder = new GsonBuilder();
        Gson g = builder.setPrettyPrinting().create();
        AllQuestion allQuestion = g.fromJson(new String(content), AllQuestion.class);
        return allQuestion;

    }

    private class MyTask extends AsyncTask<String ,String, AllQuestion> {
        @Override
        protected void onPreExecute() {

            homeProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected AllQuestion doInBackground(String... params) {

            AllQuestion allQuestion =  parseGson(params[0]);

            return allQuestion;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //updateDisplay(values[0]);
        }

        @Override
        protected void onPostExecute(AllQuestion result) {
            homeProgress.setVisibility(View.INVISIBLE);

            List<DetailsObj> list = new ArrayList<DetailsObj>();
            list.addAll(result.getQuestions());

            customAdapter = new CustomAdapter(list,getActivity());
            my_recycler_view.setAdapter(customAdapter);

        }
    }



    /*public void scheduleAlarmNotification() {


        Intent intent = new Intent(context, NotificationReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(context, NotificationReceiver.REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar =  GregorianCalendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 20);
        calendar.set(Calendar.MINUTE, 35);


        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        //String strTime= dateFormat.format(calendar.getTimeInMillis());
        //Toast.makeText(context,"Start time: "+strTime,Toast.LENGTH_SHORT).show();


        //Toast.makeText(context,"Current time: "+pref.getString("time_notification",""), Toast.LENGTH_SHORT).show();

        alarm.cancel(pIntent);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 , pIntent);

    }*/

}