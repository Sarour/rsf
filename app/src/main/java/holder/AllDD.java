package holder;

import java.util.ArrayList;
import java.util.List;

import model.DDObject;

/**
 * Created by Salahuddin on 3/7/2017.
 */

public class AllDD {

    private List<DDObject> products = new ArrayList<DDObject>();

    public List<DDObject> getProducts() {
        return products;
    }

    public void setProducts(List<DDObject> products) {
        this.products = products;
    }
}
