package holder;

import java.util.ArrayList;
import java.util.List;

import model.AlertObj;

/**
 * Created by Salahuddin on 3/13/2017.
 */

public class AllAlert {
    private List<AlertObj> AlertInfo = new ArrayList<AlertObj>();

    public List<AlertObj> getAlertInfo() {
        return AlertInfo;
    }

    public void setAlertInfo(List<AlertObj> alertInfo) {
        AlertInfo = alertInfo;
    }
}
