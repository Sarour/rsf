package holder;

import java.util.ArrayList;
import java.util.List;

import model.DescObj;

/**
 * Created by Salahuddin on 3/16/2017.
 */

public class AllDescObj {
    private List<DescObj> places = new ArrayList<DescObj>();

    public List<DescObj> getPlaces() {
        return places;
    }

    public void setPlaces(List<DescObj> places) {
        this.places = places;
    }

}
