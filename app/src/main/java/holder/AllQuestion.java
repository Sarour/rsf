package holder;

import java.util.ArrayList;
import java.util.List;

import model.DetailsObj;


public class AllQuestion {
    private List<DetailsObj> questions = new ArrayList<DetailsObj>();

    public List<DetailsObj> getQuestions() {
        return questions;
    }

    public void setQuestions(List<DetailsObj> questions) {
        this.questions = questions;
    }
}
