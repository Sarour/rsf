package db;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import model.AlertObj;


public class DBHandler extends SQLiteOpenHelper {
    private Context context;
    //DB version
    private static final int DB_VERSION = 1;
    //DB name
    private static final String DB_NAME = "db_notification";
    //Table name
    private static final String TAB_NOTIFICATION = "tab_notification";

    //TAB_NOTIFICATION columns name
    private static final String NOTIFICATION_ID="notification_id";
    private static final String NOTIFICATION_TYPE= "notification_type";
   // private static final String DEFAULT_DEVICE_USER_ID="DefaultDeviceUserId";
    private static final String NOTIFICATION_FREQUENCY = "notification_frequency";
 //   private static final String MESSAGE_CODE="MessageCode";
    //private static final String MESSAGE_TEXT = "MessageText";
    private static final String NOTIFICATION_TEXT = "notification_text";
   // private static final String NOTIFICATION_URL = "NotificationURL";
   // private static final String SERVICE_ACCESS_CODE = "ServiceAccessCode";
    private static final String NOTIFICATION_STATUS = "notification_status";
    private static final String NOTIFICATION_TIME = "notification_time";


    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_NOTIFICATION_TAB= "CREATE TABLE "
                + TAB_NOTIFICATION
                + "("
                + DBHandler.NOTIFICATION_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DBHandler.NOTIFICATION_TYPE+" TEXT,"
                //+ DBHandler.DEFAULT_DEVICE_USER_ID+" TEXT,"
                + DBHandler.NOTIFICATION_FREQUENCY+" TEXT,"
               // + DBHandler.MESSAGE_CODE+" TEXT,"
               // + DBHandler.MESSAGE_TEXT+" TEXT,"
                + DBHandler.NOTIFICATION_TEXT+" TEXT,"
               // + DBHandler.NOTIFICATION_URL+" TEXT,"
               // + DBHandler.SERVICE_ACCESS_CODE+" TEXT,"
                + DBHandler.NOTIFICATION_STATUS+" TEXT,"
                + DBHandler.NOTIFICATION_TIME+" TEXT "+")";

               db.execSQL(CREATE_NOTIFICATION_TAB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS "+TAB_NOTIFICATION);

        //Create Table again
        onCreate(db);

    }

    public DBHandler(Context context){
        super(context,DB_NAME,null,DB_VERSION);
        this.context =context;
    }


    //Insert List of Data into TAB_NOTIFICATION without duplicate data
    public void insertAllNotification(List<AlertObj> notificationData){
        final SQLiteDatabase db = this.getWritableDatabase();

        for(int i=0;i<notificationData.size();i++){
            final String isSame = " Select " + " * FROM "
                    + DBHandler.TAB_NOTIFICATION + " where "
                    + DBHandler.NOTIFICATION_TIME + "='" + notificationData.get(i).getTime() + "'";

            final Cursor cursor = db.rawQuery(isSame, null);

            if (cursor.getCount()>0){

            }else{
                String values = buildQueryValueForNotificationInsert(notificationData);
                String query = "INSERT INTO " + DBHandler.TAB_NOTIFICATION+ "("
                        + DBHandler.NOTIFICATION_TYPE+", "
                       // + DBHandler.DEFAULT_DEVICE_USER_ID+", "
                        + DBHandler.NOTIFICATION_FREQUENCY +", "
                        //+ DBHandler.MESSAGE_CODE+", "
                       // + DBHandler.MESSAGE_TEXT+", "
                        + DBHandler.NOTIFICATION_TEXT+", "
                       // + DBHandler.NOTIFICATION_URL+", "
                       // + DBHandler.SERVICE_ACCESS_CODE+", "
                        + DBHandler.NOTIFICATION_STATUS+", "
                        + DBHandler.NOTIFICATION_TIME+") VALUES " + values;
                db.execSQL(query);
            }
            cursor.close();
        }

        db.close();
    }

    private String buildQueryValueForNotificationInsert(List<AlertObj> nData){
        //String array of size equals to list size
        String[] valueArray = new String[nData.size()];

        for(int i = 0; i < nData.size(); i++){
            // Fetching model from list one by one
            AlertObj data = nData.get(i);

            String type = data.getAlertType();
           // String device_url = data.getDefaultDeviceUserID();
            String frequency = data.getFrequency();
           // String message_code =data.getMessageCode();
           // String message_text = data.getMessageText();
            String text = data.getNotificationText();
           // String notification_url =data.getNotificationURL();
           // String access_code = data.getServiceAccessCode();
            String status = data.getStatus();
            String time = ""+ data.getTime();

            String[] values = new String[]{type,frequency,text,status,time};
            // Pass the string array in buildQueryInsertValue
            valueArray[i] = buildQueryInsertValue(values);
        }
        return TextUtils.join(",", valueArray);
    }

    private String buildQueryInsertValue(String[] values){
        for(int i = 0; i < values.length; i++){
            if(values[i] == null){
                values[i] = "";
            }
            values[i] = DatabaseUtils.sqlEscapeString(values[i]);
        }
        return "(" + TextUtils.join(",", values) + ")";


    }
//End of Insert List of Data into TAB_FLOWER


    //All message read from db.
      public List<AlertObj> getAllMessage() {
        List<AlertObj> notificationList = new ArrayList<AlertObj>();
        // Select All Query
        String selectAllQuery = "SELECT * FROM " + DBHandler.TAB_NOTIFICATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectAllQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AlertObj notification = new AlertObj();
                notification.setAlertType(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TYPE)));
                notification.setFrequency(cursor.getString(cursor.getColumnIndex(NOTIFICATION_FREQUENCY)));
                notification.setNotificationText(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TEXT)));
                notification.setStatus(cursor.getString(cursor.getColumnIndex(NOTIFICATION_STATUS)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TIME)));
                // Adding person to list
                notificationList.add(notification);
            } while (cursor.moveToNext());
        }
          cursor.close();
          db.close();
         return notificationList;
    }

    //Is time include in table?
     public boolean isTime(String time) {
        boolean flag = false;
        final SQLiteDatabase db = this.getWritableDatabase();

        final String queryIs = "Select " + "* FROM "
                + DBHandler.TAB_NOTIFICATION + " where "
                + DBHandler.NOTIFICATION_TIME + "='"+time+"'"
                +" AND "+DBHandler.NOTIFICATION_STATUS+"="+0+"";
         Log.e("check data query: "+queryIs,">>");

        final Cursor cursor = db.rawQuery(queryIs, null);

        if (cursor.getCount()>0) {
            flag = true;
        }else{
            flag = false;
        }

        cursor.close();
        db.close();
        return flag;
    }

    //Delete single message
        public boolean removeSingleMessage(String time) {
        boolean isSuccess = false;
        SQLiteDatabase db = this.getWritableDatabase();
        isSuccess = db.delete(DBHandler.TAB_NOTIFICATION, DBHandler.NOTIFICATION_TIME + "='" + time + "'", null) > 0;
        db.close();
        return isSuccess;
    }
    //Delete all message
       public void deleteAllMessage(){
        final SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ DBHandler.TAB_NOTIFICATION);
        db.close();
    }

     public boolean updateMessage(String time) {
        boolean flag = false;
        final SQLiteDatabase db = getWritableDatabase();

        final String queryStr = "Select " + "* FROM "
                + DBHandler.TAB_NOTIFICATION + " where "
                + DBHandler.NOTIFICATION_TIME + "='" + time + "'";

        final Cursor cursor = db.rawQuery(queryStr, null);

        if (cursor.getCount() > 0) {
            final String queryStrUpdate= "Update "
                    + DBHandler.TAB_NOTIFICATION + " set "
                    + DBHandler.NOTIFICATION_STATUS + "='" + 1 + "'"
                    + " where " + DBHandler.NOTIFICATION_TIME + "='" + time + "'";
            Log.e("update ", ">>" + queryStrUpdate);
            db.execSQL(queryStrUpdate);
            flag = true;
        }
         cursor.close();
        db.close();
        return flag;
    }


    public AlertObj getSingleMessage(String time) {
        // Select Object Query
        final String query = "Select " + "* FROM "
                + DBHandler.TAB_NOTIFICATION + " where "
                + DBHandler.NOTIFICATION_TIME + "='"+time+"'"
                +" AND " + DBHandler.NOTIFICATION_STATUS + "='"+0+"'";
        Log.e("single Obj query"+query,">>>");

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        AlertObj notification = new AlertObj();
        if (cursor.moveToFirst()) {
            do {

                notification.setAlertType(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TYPE)));
                notification.setFrequency(cursor.getString(cursor.getColumnIndex(NOTIFICATION_FREQUENCY)));
                notification.setNotificationText(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TEXT)));
                notification.setStatus(cursor.getString(cursor.getColumnIndex(NOTIFICATION_STATUS)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TIME)));

            } while (cursor.moveToNext());
        }


        cursor.close();
        db.close();
        return notification;
    }



    public long getTotalItemNotify() {
        long totalNumber = 0;
        final SQLiteDatabase db = getWritableDatabase();

        final String totalCountQuery = "SELECT "
                + "SUM("+DBHandler.NOTIFICATION_STATUS+") as TotalCount FROM "+DBHandler.TAB_NOTIFICATION
                +" where "+DBHandler.NOTIFICATION_STATUS+"='"+1+"'";
        Log.e("notify item: "+totalCountQuery,">>");

         Cursor cursor = db.rawQuery(totalCountQuery, null);

        if (cursor.moveToFirst()) {

            totalNumber = cursor.getInt(cursor.getColumnIndex("TotalCount"));

        }

        Log.e("TotalCount>>>", totalNumber + "");
        cursor.close();
        db.close();

        return totalNumber;
    }


   public long getTotalRows() {
        long totalNumber = 0;
        final SQLiteDatabase db = getWritableDatabase();
        totalNumber = DatabaseUtils.longForQuery(db, " SELECT " + "COUNT(" + "*" + ")" + " FROM " + DBHandler.TAB_NOTIFICATION, null);
        db.close();
        return totalNumber;
    }

    //All message read from db.
    public List<AlertObj> getAllAfterNotification() {
        List<AlertObj> notificationList = new ArrayList<AlertObj>();
        // Select All Query
        String selectAllNotificationMgs = "SELECT * FROM " + DBHandler.TAB_NOTIFICATION
                +" where "+DBHandler.NOTIFICATION_STATUS+"='"+1+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectAllNotificationMgs, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AlertObj notification = new AlertObj();
                notification.setAlertType(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TYPE)));
                notification.setFrequency(cursor.getString(cursor.getColumnIndex(NOTIFICATION_FREQUENCY)));
                notification.setNotificationText(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TEXT)));
                notification.setStatus(cursor.getString(cursor.getColumnIndex(NOTIFICATION_STATUS)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TIME)));
                // Adding person to list
                notificationList.add(notification);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return notificationList;
    }


}