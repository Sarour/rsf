package broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import utils.AppConstant;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (AppConstant.isOnline(context)) {

            Toast.makeText(context,"Is  Online",Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(context,"Is  Offline",Toast.LENGTH_SHORT).show();
        }
    }
}