package broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import appservice.NotificationService;

import static android.content.Context.MODE_PRIVATE;

public class NotificationReceiver extends BroadcastReceiver {
    public static final int REQ_CODE = 54321;

    @Override
    public void onReceive(Context context, Intent intent) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date now = new Date();


        final SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        //First time save
        if (!pref.getString("time","").equalsIgnoreCase("F_time")){

            editor.putString("time","F_time");
            editor.putLong("first_tiger",System.currentTimeMillis());

            editor.commit();
        }


        Calendar calendar =  Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.setTime(now);

        int unroundedMinutes = calendar.get(Calendar.MINUTE);
        int mod = unroundedMinutes % 10;
        calendar.set(Calendar.MINUTE, mod < 10 && mod>0 ? (unroundedMinutes-mod) : unroundedMinutes);
        calendar.set(Calendar.SECOND, 00);
        calendar.set(Calendar.MILLISECOND, 00);


        editor.putString("time_notification",dateFormat.format(calendar.getTimeInMillis()));
        editor.commit();

        Log.e("start time: "+pref.getString("time_notification",""),">>>>");

            intent = new Intent(context, NotificationService.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(NotificationService.ACTION);
            context.startService(intent);
            Log.e("call service: ",">>>>>");


    }


}