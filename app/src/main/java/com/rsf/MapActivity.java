package com.rsf;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import holder.AllDescObj;
import model.DescObj;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private Context context;
    private Toolbar toolbarMap;
    private ImageView ivBackMap;
    private GoogleMap mMap;


   private ArrayList<DescObj> list = new ArrayList<DescObj>();
   private PolylineOptions polylineOptions;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        context =this;
         toolbarMap = (Toolbar) findViewById(R.id.toolbarMap);


        String jsonString= loadJSONFromAsset();
        AllDescObj allDescObj = parseGson(jsonString);
        if (list!=null)
            list.clear();
        list.addAll(allDescObj.getPlaces());


        for(int i = 0;i<list.size();i++){
            DescObj descObj = list.get(i);
            Log.e("lat"+ descObj.getLat(),">>>>");
            Log.e("long"+ descObj.getLng(),">>>>");
        }


        intUit();

    }

    private void intUit() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ivBackMap = (ImageView) findViewById(R.id.ivBackMap);
        ivBackMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public String loadJSONFromAsset() {

        String json = null;
        try {
            InputStream is =getAssets().open("location.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
    private AllDescObj parseGson(String content){

        GsonBuilder builder = new GsonBuilder();
        Gson g = builder.setPrettyPrinting().create();
        AllDescObj allDescObj = g.fromJson(new String(content), AllDescObj.class);
        return allDescObj;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        inuitMap(mMap);
    }


    private void inuitMap(final GoogleMap googleMap) {

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        googleMap.setMyLocationEnabled(false);


        final ArrayList<LatLng> listLatLong = new ArrayList<LatLng>();
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pic_icon);

        polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.RED);
        polylineOptions.width(5);

                 for (int i = 0;i<list.size();i++){
                     DescObj descObj = list.get(i);
                     LatLng latLng = new LatLng(Double.parseDouble(descObj.getLat()),Double.parseDouble(descObj.getLng()));
                     googleMap.addMarker(new MarkerOptions().position(latLng)
                             .snippet(descObj.getMobile())
                             .icon(icon)
                             .title(descObj.getAddress()));
                     listLatLong.add(latLng);

                     polylineOptions.add(latLng);

                 }
                googleMap.addPolyline(polylineOptions);




        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                try {
                    final String uri = "tel:" + marker.getSnippet();
                    if (!TextUtils.isEmpty(uri)) {
                        final Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
                        startActivity(dialIntent);
                    }
                } catch (final Exception e) {

                    e.printStackTrace();
                }
            }
        });

             final Handler handler = new Handler();
              handler.postDelayed(new Runnable() {
              @Override
              public void run() {
                zoomToCoverAllMarkers(listLatLong, googleMap);

            }
        }, 500);


    }


    private  void zoomToCoverAllMarkers(ArrayList<LatLng> latLngList, GoogleMap googleMap) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng marker : latLngList) {
            builder.include(marker);
        }
        LatLngBounds bounds = builder.build();
        int padding = 90;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.moveCamera(cu);
        googleMap.animateCamera(cu);
    }


}



