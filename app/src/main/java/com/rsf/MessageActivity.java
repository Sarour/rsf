package com.rsf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapter.MgsAdapter;
import db.DBHandler;
import holder.AllAlert;
import model.AlertObj;

public class MessageActivity extends AppCompatActivity {
    private Context context;
    private Activity mActivity;
    private Toolbar toolbarMessage;
    private ImageView ivBackMessage;
    private AllAlert allAlert = new AllAlert();
    private List<AlertObj> listAlert = new ArrayList<AlertObj>();
    private TextView tvMessageNotFound;
    private RecyclerView message_recycler;
    private RecyclerView.LayoutManager layoutManager;
    private MgsAdapter mgsAdapter;
    private DBHandler dbHandler;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        context =this;
        dbHandler = new DBHandler(context);
        mActivity=  this;
        toolbarMessage = (Toolbar) findViewById(R.id.toolbarMessage);
         setSupportActionBar(toolbarMessage);
         getSupportActionBar().setDisplayShowTitleEnabled(true);

        intUit();
    }

    private void intUit() {

        tvMessageNotFound = (TextView) findViewById(R.id.tvMessageNotFound);
        message_recycler = (RecyclerView) findViewById(R.id.message_recycler);
        message_recycler.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);
        message_recycler.setLayoutManager(layoutManager);
        message_recycler.setItemAnimator(new DefaultItemAnimator());

         //dbHandler.getTotalItemNotify();

        if (listAlert!=null)
            listAlert.clear();

       // final SharedPreferences pref = getSharedPreferences("MyPref", MODE_PRIVATE);
       // final SharedPreferences.Editor editor = pref.edit();


      /*   GsonBuilder builder = new GsonBuilder();
         Gson g = builder.setPrettyPrinting().create();
         allAlert = g.fromJson(pref.getString("res_notification", ""), AllAlert.class);*/

        listAlert.addAll(dbHandler.getAllMessage());
         Log.e("total row size: "+dbHandler.getTotalRows(),"<<<<<<<");

        if (listAlert.size()>0){
            tvMessageNotFound.setVisibility(View.GONE);
        }else{
            tvMessageNotFound.setVisibility(View.VISIBLE);
        }

        mgsAdapter = new MgsAdapter(listAlert,mActivity);
        message_recycler.setAdapter(mgsAdapter);




        ivBackMessage = (ImageView) findViewById(R.id.ivBackMessage);
        ivBackMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}