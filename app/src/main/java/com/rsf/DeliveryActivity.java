package com.rsf;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import adapter.DRoutSpAd;
import adapter.HeaderAdapter;
import holder.AllDD;
import model.DDObject;

public class DeliveryActivity extends AppCompatActivity {
    private Context context;
    private Toolbar toolbarDelivery;
    private ImageView ivBackDelivery;
    private Spinner dRouteNo,dSaleId,dRetailer;
    private EditText etPaymentStatus;
    private RecyclerView listItem;
    private HeaderAdapter headerAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private CheckBox checkScanner;
    private Button subBtn;
    private LinearLayout motherLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        context =this;

        toolbarDelivery = (Toolbar) findViewById(R.id.toolbarDelivery);
         setSupportActionBar(toolbarDelivery);
         getSupportActionBar().setDisplayShowTitleEnabled(true);

        intUit();
    }

    private void intUit() {
        final SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        checkScanner = (CheckBox) findViewById(R.id.checkScanner);
        etPaymentStatus = (EditText) findViewById(R.id.etPaymentStatus);
        etPaymentStatus.setFocusable(false);
        dRouteNo = (Spinner) findViewById(R.id.dRouteNo);
        dSaleId = (Spinner) findViewById(R.id.dSaleId);
        dRetailer = (Spinner) findViewById(R.id.dRetailer);

        motherLayout = (LinearLayout) findViewById(R.id.motherLayout);
        subBtn = (Button) findViewById(R.id.subBtn);


        LayoutTransition transition = new LayoutTransition();
        motherLayout.setLayoutTransition(transition);


        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int childCount = motherLayout.getChildCount();

                for (int i = 0; i < childCount; i++) {
                    final View child = motherLayout.getChildAt(i);
                    TextView tvProduct = (TextView) (child.findViewById(R.id.tvProduct));
                    //TextView tvQuantity = (TextView) (child.findViewById(R.id.tvQuantity));
                    EditText tvDeliveryQuantity = (EditText) (child.findViewById(R.id.tvDeliveryQuantity));


                    //Toast.makeText(context,"D>>"+tvDeliveryQuantity.getText().toString(),Toast.LENGTH_SHORT).show();



                    LinearLayout addInnerView = (LinearLayout) (child.findViewById(R.id.addViewSerial));
                    int cc = addInnerView.getChildCount();


               if(cc>1) {
                   Toast.makeText(context,"inner size: "+cc,Toast.LENGTH_SHORT).show();
                   for (int index = i; index < cc; index++) {
                       View c = addInnerView.getChildAt(index);

                       View layout = getLayoutInflater().inflate(R.layout.row, null);

                       try {
                           TextView textOut = (TextView) (c.findViewById(R.id.textout));
                           Toast.makeText(context, "show: " + textOut.getText().toString(), Toast.LENGTH_SHORT).show();

                       } catch (Exception e) {

                       }
                   }
               }


                }





            }
        });


       // listItem = (RecyclerView) findViewById(R.id.listItem);
        //listItem.setHasFixedSize(true);

       // layoutManager = new LinearLayoutManager(context);
       // listItem.setLayoutManager(layoutManager);
       // listItem.setItemAnimator(new DefaultItemAnimator());

        if (checkScanner.isChecked()){
            editor.putBoolean("using_scanner",true);
            editor.commit();
        }else {
            editor.putBoolean("using_scanner",false);
            editor.commit();
        }

        checkScanner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                    editor.putBoolean("using_scanner",true);
                    editor.commit();
                    Toast.makeText(context,"Check: "+pref.getBoolean("using_scanner",false),Toast.LENGTH_SHORT).show();

                }else{

                    editor.putBoolean("using_scanner",false);
                    editor.commit();
                    Toast.makeText(context,"Uncheck: "+pref.getBoolean("using_scanner",false),Toast.LENGTH_SHORT).show();
                }
            }
        });
        ArrayList<String> routeNo = new ArrayList<String>();
        routeNo.add("Select");
        routeNo.add("Route-1");
        routeNo.add("Route-2");
        routeNo.add("Route-3");
        routeNo.add("Route-4");
        routeNo.add("Route-5");
        routeNo.add("Route-6");
        routeNo.add("Route-7");
        routeNo.add("Route-8");
        routeNo.add("Route-9");
        routeNo.add("Route-11");
        routeNo.add("Route-22");
        routeNo.add("Route-33");
        routeNo.add("Route-44");
        routeNo.add("Route-55");
        routeNo.add("Route-66");
        routeNo.add("Route-77");
        routeNo.add("Route-88");
        routeNo.add("Route-99");
        routeNo.add("Route-111");
        routeNo.add("Route-222");
        routeNo.add("Route-333");
        routeNo.add("Route-444");
        routeNo.add("Route-555");
        routeNo.add("Route-666");
        routeNo.add("Route-777");
        routeNo.add("Route-888");

        DRoutSpAd dRoutSpAd=new DRoutSpAd(context,routeNo);
        dRouteNo.setAdapter(dRoutSpAd);

        dSaleId.setAdapter(dRoutSpAd);
        dRetailer.setAdapter(dRoutSpAd);


        ivBackDelivery = (ImageView) findViewById(R.id.ivBackDelivery);
        ivBackDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

     /*   btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BarcodeScanner.class);
                startActivity(intent);
            }
        });*/


        String jsonString= loadJSONFromAsset();
        MyTask task = new MyTask();
        task.execute(jsonString);
    }



    public String loadJSONFromAsset() {

        String json = null;
        try {
            InputStream is = getAssets().open("delivery.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    private AllDD parseGson(String content){
        GsonBuilder builder = new GsonBuilder();
        Gson g = builder.setPrettyPrinting().create();
        AllDD alldd = g.fromJson(new String(content), AllDD.class);
        return alldd;
    }



    private class MyTask extends AsyncTask<String ,String, AllDD> {
        @Override
        protected void onPreExecute() {

           // homeProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected AllDD doInBackground(String... params) {

            AllDD allDD =  parseGson(params[0]);

            return allDD;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //updateDisplay(values[0]);
        }

        @Override
        protected void onPostExecute(AllDD result) {
           // homeProgress.setVisibility(View.INVISIBLE);

            final List<DDObject> list = new ArrayList<DDObject>();
            list.addAll(result.getProducts());

            int sizeList = list.size();


            addViewRow(sizeList,list);
            //headerAdapter = new HeaderAdapter(list,context);
            //listItem.setAdapter(headerAdapter);

        }
    }
    protected void addViewRow(int sizeList,List<DDObject> list){

        for (int i=0;i<sizeList;i++){

            View view = LayoutInflater.from(context).inflate(R.layout.item_row,null);
            final DDObject dataItem =list.get(i);
            final HashSet<String> hashSet = new HashSet<String>();

            if(view.getParent()!=null){
                ((ViewGroup)view.getParent()).removeView(view);
            }

            TextView tvProduct = (TextView) view.findViewById(R.id.tvProduct);
            TextView tvQuantity = (TextView) view.findViewById(R.id.tvQuantity);
            EditText tvDeliveryQuantity = (EditText) view.findViewById(R.id.tvDeliveryQuantity);
            final EditText tvSerial = (EditText) view.findViewById(R.id.tvSerial);
            ImageView addSerialBtn = (ImageView) view.findViewById(R.id.addSerialBtn);
            final LinearLayout addViewSerial = (LinearLayout) view.findViewById(R.id.addViewSerial);

            tvProduct.setText(dataItem.getDetail().getP_name());
            tvQuantity.setText(dataItem.getDetail().getP_order_quantity());
            tvDeliveryQuantity.setText(dataItem.getDetail().getP_delivery_quantity());
            tvSerial.setText(dataItem.getDetail().getP_serial_quantity());

            //final SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
            //final SharedPreferences.Editor editor = pref.edit();

            //if (pref.getBoolean("using_scanner",true)){

               addSerialBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.row, null);

                    TextView textOut = (TextView)addView.findViewById(R.id.textout);
                    final String serialNumber = tvSerial.getText().toString().trim();

                    if (!TextUtils.isEmpty(serialNumber) && (!hashSet.contains(serialNumber))){
                        hashSet.add(serialNumber);
                        textOut.setText(serialNumber);
                        tvSerial.setText("");
                        addViewSerial.addView(addView);

                        for (String s: hashSet) {
                            Log.e(">>>",""+s);
                        }

                    }else{
                        Toast.makeText(context,"Already added or empty field",Toast.LENGTH_SHORT).show();
                    }

                    ImageView buttonRemove = (ImageView) addView.findViewById(R.id.remove);
                    buttonRemove.setOnClickListener(new View.OnClickListener(){

                        @Override
                        public void onClick(View v) {
                            hashSet.remove(serialNumber);
                            ((LinearLayout)addView.getParent()).removeView(addView);

                            for (String s: hashSet) {
                                Log.e(">>>",""+s);
                            }
                        }});
                }
            });




            //}


            motherLayout.addView(view);

        }
    }


}