package com.rsf;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import adapter.MgsAdapter;
import db.DBHandler;
import model.AlertObj;

public class SalesCallActivity extends AppCompatActivity {
    private Context context;
    private Toolbar toolbarSalesCall;
    private ImageView ivBackSalesCall;
    private RecyclerView horizontal_recycler;
    private RecyclerView.LayoutManager layoutManager;
    private MgsAdapter mgsAdapter;
    private DBHandler dbHandler;
    private List<AlertObj> listAlert = new ArrayList<AlertObj>();
    private Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salescall);
        context =this;
        mActivity =this;
        dbHandler = new DBHandler(context);
        toolbarSalesCall = (Toolbar) findViewById(R.id.toolbarSalesCall);
         setSupportActionBar(toolbarSalesCall);
         getSupportActionBar().setDisplayShowTitleEnabled(true);

        intUit();
    }

    private void intUit() {
        ivBackSalesCall = (ImageView) findViewById(R.id.ivBackSalesCall);
        ivBackSalesCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        horizontal_recycler = (RecyclerView) findViewById(R.id.horizontal_recycler);
        horizontal_recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        horizontal_recycler.setLayoutManager(layoutManager);
        horizontal_recycler.setItemAnimator(new DefaultItemAnimator());

        if (listAlert!=null)
            listAlert.clear();

        listAlert.addAll(dbHandler.getAllAfterNotification());

        mgsAdapter = new MgsAdapter(listAlert,mActivity);
        horizontal_recycler.setAdapter(mgsAdapter);

    }
}