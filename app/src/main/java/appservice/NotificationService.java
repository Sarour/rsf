package appservice;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.rsf.MainActivity;
import com.rsf.R;

import broadcast.NotificationReceiver;
import db.DBHandler;
import model.AlertObj;

public class NotificationService extends IntentService {
    private DBHandler dbHandler;
    private NotificationReceiver notificationReceiver;
    public final static String ACTION = "NotifyServiceAction";
    private boolean getTime= false;
    private long millis;

   public NotificationService() {
        super("NotificationService");
    }

    @Override
    public void onCreate() {
        notificationReceiver = new NotificationReceiver();
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.e("onStart notification", "onStart()");
        super.onStart(intent, startId);

    }
    @Override
    protected void onHandleIntent(Intent intent) {

        dbHandler = new DBHandler(getApplicationContext());

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION);
        registerReceiver(notificationReceiver, intentFilter);

        millis = System.currentTimeMillis();

           startNotification();

    }

    private void startNotification(){

                getTime = isTimeGet();
                Log.e("getTime: "+getTime,">>>");
                notificationTo();
    }

    private synchronized boolean isTimeGet(){
        boolean flag = false;
        final SharedPreferences pref = getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();
        String time= pref.getString("time_notification", "");

        try {
            flag=  dbHandler.isTime(time);
           }catch (NullPointerException e){
            e.printStackTrace();
           }

        Log.e("Flag: "+flag,"<<<");
        if (flag){
            return true;
        }else{
            return false;
        }
    }

    private synchronized  void notificationTo(){

        final SharedPreferences pref = getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();
        String time= pref.getString("time_notification", "");

        if (!TextUtils.isEmpty(time)) {

            long totalRows = 0L;
            long totalRead = 0L;
            try {
                  totalRows = dbHandler.getTotalRows();
                  totalRead = dbHandler.getTotalItemNotify();
            }catch (NullPointerException e){
                e.printStackTrace();
            }

            if (getTime) {
                AlertObj alertObj = dbHandler.getSingleMessage(time);
                dbHandler.updateMessage(time);

                Intent ii = new Intent(getApplicationContext(), MainActivity.class);
                ii.addFlags(ii.FLAG_ACTIVITY_CLEAR_TOP);

                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), ii, 0);

                long[] pattern = {500, 500, 500, 500, 500};

                // Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                //Uri path = Uri.parse("android.resource://"+getPackageName()+"/raw/loudest.mp3");
                //RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_RINGTONE,path);

                NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getApplicationContext()).setSmallIcon(R.drawable.cast_ic_notification_2)
                        .setContentTitle("New message here please read this...")
                        .setContentText(alertObj.getNotificationText())
                        .setAutoCancel(true)
                        .setVibrate(pattern)
                        .setLights(Color.BLUE, 1, 1)
                       // .setSound(defaultSoundUri)
                        .setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.loudest))
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0, notificationBuilder.build());
            }else if(((pref.getLong("first_tiger",0))+(24*60*60*1000))< millis){

                  editor.putLong("first_tiger", millis);
                  editor.commit();
                  dbHandler.deleteAllMessage();

            }
        }
    }

   @Override
    public void onDestroy() {
        unregisterReceiver(notificationReceiver);
        super.onDestroy();
        Log.e("notification service: ","onDestroy");
    }
}