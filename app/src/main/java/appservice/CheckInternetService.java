package appservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import utils.AppConstant;


public class CheckInternetService extends Service {
    public static  boolean flag = false;


    final class MyThread implements Runnable{

        int service_id;
        Context context;
        MyThread(Context context,int service_id){
            this.context = context;
            this.service_id = service_id;
        }

        @Override
        public void run() {

            flag = AppConstant.isOnline(context);
            Intent broadcastIntent = new Intent("unique_message");
            broadcastIntent.putExtra("message", flag);
            sendBroadcast(broadcastIntent);

        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Context context = getApplicationContext();
        Thread thread = new Thread(new MyThread(context,startId));
        thread.start();
        return super.onStartCommand(intent, flags, startId);
    }
}
