package appservice;

import android.app.IntentService;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import broadcast.MyAlarmReceiver;
import cz.msebera.android.httpclient.Header;
import db.DBHandler;
import holder.AllAlert;
import model.AlertObj;
import utils.AppConstant;

public class NetworkedIntentService extends IntentService {

    private static final String URL = "https://202.164.211.110/RASolarERPWebServices_LiveTest/RASolarERP_AlertManagement.svc/AlertOrNotification";
    private AllAlert allAlert;
    private List<AlertObj> listAlert = new ArrayList<AlertObj>();
    private DBHandler dbHandler;
    private AsyncHttpClient aClient = new SyncHttpClient();
    private MyAlarmReceiver myAlarmReceiver;
    public final static String ACTION = "NetworkedIntentServiceAction";

    public NetworkedIntentService() {
        super("NetworkedIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myAlarmReceiver = new MyAlarmReceiver();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.e("onStart Service", "onStart()");
        super.onStart(intent, startId);

    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        dbHandler = new DBHandler(getApplicationContext());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION);
        registerReceiver(myAlarmReceiver, intentFilter);

        Thread thread = new Thread(new MyNetWorkSevice());
                   thread.start();

    }

    private String loadJSONFromAsset() {

        String json = null;
        try {
            InputStream is =getAssets().open("notific.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private AllAlert parseGson(String content){

        GsonBuilder builder = new GsonBuilder();
        Gson g = builder.setPrettyPrinting().create();
        AllAlert allAlert = g.fromJson(new String(content), AllAlert.class);
        return allAlert;
    }

    final class MyNetWorkSevice implements Runnable{

        @Override
        public void run() {


            //Local Check only Testing purpose
   /*        if (listAlert!=null){
            listAlert.clear();
        }
        String jsonString= loadJSONFromAsset();
        Log.e("json String"+jsonString,"<<<");
        AllAlert allAlert = parseGson(jsonString);

        if (allAlert!=null){

            for (int i=0;i<allAlert.getAlertInfo().size();i++){

                AlertObj alertObj = new AlertObj();
                alertObj.setAlertType(allAlert.getAlertInfo().get(i).getAlertType());


                alertObj.setTime(allAlert.getAlertInfo().get(i).getTime());
                alertObj.setStatus(allAlert.getAlertInfo().get(i).getStatus());
                alertObj.setNotificationText(allAlert.getAlertInfo().get(i).getNotificationText());
                alertObj.setFrequency(allAlert.getAlertInfo().get(i).getFrequency());
                listAlert.add(alertObj);
            }

            dbHandler.insertAllNotification(listAlert);

        }*/

          //Online Data received

        if (AppConstant.isOnline(getApplicationContext())) {
              Log.e("Connection>>>","OK...");

        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("strDeviceID", "356096060016845");
        paramMap.put("strAlertType", "Notification");
        RequestParams params = new RequestParams(paramMap);

        aClient.get(URL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
                if (listAlert.size()>0)
                 listAlert.clear();
                //Intent broadcast = new Intent(getApplicationContext(),MessageActivity.class);
                Log.d("onStart method", "onStart");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {

                Log.e("response notification", ">>>>>>>" + new String(response));

                String res = new String(response);
                GsonBuilder builder = new GsonBuilder();
                Gson g = builder.setPrettyPrinting().create();
                allAlert = g.fromJson(res, AllAlert.class);

                if (allAlert!=null){
                    for (int i=0;i<allAlert.getAlertInfo().size();i++){
                        AlertObj alertObj = new AlertObj();
                        alertObj.setAlertType(allAlert.getAlertInfo().get(i).getAlertType());
                        alertObj.setTime(allAlert.getAlertInfo().get(i).getTime());
                        alertObj.setStatus(allAlert.getAlertInfo().get(i).getStatus());
                        alertObj.setNotificationText(allAlert.getAlertInfo().get(i).getNotificationText());
                        alertObj.setFrequency(allAlert.getAlertInfo().get(i).getFrequency());
                        listAlert.add(alertObj);
                    }
                    dbHandler.insertAllNotification(listAlert);
                }
                Log.e("onSuccess method>>API", "onSuccess");
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.e("onFailure>>API", "onFailure");
            }

            @Override
            public void onCancel() {
                Log.e("onCancel method>>API", "onCancel");
            }
            @Override
            public void onRetry(int retryNo) {
                Log.e("onRetry method>>API", String.format("onRetry: %d", retryNo));
            }
            @Override
            public void onFinish() {
                Log.e("onFinish method>>API", "onFinish");
            }
        });

        }else{
          Log.e("Connection>>>","Problem");
         }



        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(myAlarmReceiver);
        super.onDestroy();
        Log.e("Network service: ","onDestroy");
        // dbHandler.close();
    }
}