package dialog;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsf.R;

import utils.AppConstant;


public class MessageDialog extends DialogFragment {
    private Context context;
    private View view;
    private Toolbar toolbarMessageDialog;
    private ImageView ivBackDMessage;
    private TextView titleMessage,descMessage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_message, container, true);
        context = getActivity();
        toolbarMessageDialog = (Toolbar) view.findViewById(R.id.toolbarMessageDialog);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbarMessageDialog);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);



        initUi();
        return view;
    }
    private void initUi() {


        titleMessage =(TextView) view.findViewById(R.id.titleMessage);
        descMessage =(TextView) view.findViewById(R.id.descMessage);

        if (!TextUtils.isEmpty(AppConstant.TITLE)){
            titleMessage.setText(AppConstant.TITLE);
        }else{
            titleMessage.setText("");
        }

        if (!TextUtils.isEmpty(AppConstant.DESC)){
            descMessage.setText(AppConstant.DESC);
        }else{
            descMessage.setText("");
        }

        ivBackDMessage = (ImageView) view.findViewById(R.id.ivBackDMessage);


        ivBackDMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

    }

}
