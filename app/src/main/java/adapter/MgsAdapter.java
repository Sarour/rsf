package adapter;

import android.app.Activity;
import android.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rsf.R;

import java.util.ArrayList;
import java.util.List;

import dialog.MessageDialog;
import model.AlertObj;
import utils.AppConstant;


public class MgsAdapter extends RecyclerView.Adapter<MgsAdapter.MyViewHolder> {

    private List<AlertObj> listAlert = new ArrayList<AlertObj>();
    private Activity context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        protected TextView mgsTitle;
        protected TextView mgsTime;
        protected TextView messageDesc;
        protected View mRootView;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.mgsTitle = (TextView) itemView.findViewById(R.id.mgsTitle);
            this.mgsTime = (TextView) itemView.findViewById(R.id.mgsTime);
            this.messageDesc = (TextView) itemView.findViewById(R.id.messageDesc);
            mRootView = itemView;

        }
    }

    public MgsAdapter(List<AlertObj> listAlert, Activity context) {
        this.listAlert = listAlert;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_row, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView mgsTitle = holder.mgsTitle;
        TextView mgsTime  = holder.mgsTime;
        TextView messageDesc  = holder.messageDesc;

        final AlertObj itemData = listAlert.get(listPosition);

        if (!TextUtils.isEmpty(itemData.getStatus())){
            mgsTitle.setText(itemData.getStatus());
        }else {
            mgsTitle.setText("");
        }

        if (!TextUtils.isEmpty(itemData.getTime())){
            mgsTime.setText(itemData.getTime());
        }else {
            mgsTime.setText("");
        }

        if (!TextUtils.isEmpty(itemData.getNotificationText())){
            messageDesc.setText(itemData.getNotificationText());
        }else {
            messageDesc.setText("");
        }


        //iconLabel.setText("\u2302");
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            /*    if (TextUtils.isEmpty(itemData.getMessageText())){

                }else{
                    AppConstant.TITLE = itemData.getMessageText();
                }*/

                if (TextUtils.isEmpty(itemData.getNotificationText())){

                }else{
                    AppConstant.DESC =itemData.getNotificationText();
                }


                MessageDialog dialogBooking = new MessageDialog();
                dialogBooking.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen);
                dialogBooking.show(context.getFragmentManager(), "");

            }
        });


    }

    @Override
    public int getItemCount() {
        return listAlert.size();
    }

}
