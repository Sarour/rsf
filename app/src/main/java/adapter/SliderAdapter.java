package adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rsf.R;


/**
 * Created by User on 9/28/2016.
 */

public class SliderAdapter extends PagerAdapter {
    public Context con;
    int[] imageRSC;
    String[] titleArray, desArray;
    // constructor
    public SliderAdapter(Context con, int[] imageRSC, String[] titleArray, String[] desArray) {
        this.con = con;
        this.imageRSC = imageRSC;
        this.titleArray = titleArray;
        this.desArray = desArray;
    }
    @Override
    public int getCount() {
        return this.imageRSC.length;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.pager_row, container, false);
        TextView textViewTitle=(TextView)viewLayout.findViewById(R.id.textViewTitle);
        TextView textViewDetails=(TextView)viewLayout.findViewById(R.id.textViewDetails);
        ImageView imageviewSlider=(ImageView)viewLayout.findViewById(R.id.imageviewSlider);
        textViewTitle.setText(titleArray[position]);
        textViewDetails.setText(desArray[position]);
        imageviewSlider.setImageResource(imageRSC[position]);

        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}
