package adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rsf.DeliveryActivity;
import com.rsf.MapActivity;
import com.rsf.MessageActivity;
import com.rsf.R;
import com.rsf.SalesActivity;
import com.rsf.SalesCallActivity;

import java.util.ArrayList;
import java.util.List;

import model.DetailsObj;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private List<DetailsObj> listQuestion = new ArrayList<DetailsObj>();
    private Activity context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        protected TextView textIcon;
        protected TextView txQTitle;
        protected View mRootView;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.textIcon = (TextView) itemView.findViewById(R.id.textIcon);
            this.txQTitle = (TextView) itemView.findViewById(R.id.txQTitle);
            mRootView = itemView;

        }
    }

    public CustomAdapter(List<DetailsObj> listQuestion, Activity context) {
        this.listQuestion = listQuestion;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_row, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textIcon = holder.textIcon;
        TextView txQTitle  = holder.txQTitle;


        final DetailsObj qd = listQuestion.get(listPosition);

        Typeface fontAwesomeFont = Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome-webfont.ttf");
        textIcon.setTypeface(fontAwesomeFont);

        textIcon.setText(qd.getQdetail().getDate());
        if (!TextUtils.isEmpty(qd.getQdetail().getTitle())){
            txQTitle.setText(qd.getQdetail().getTitle());
        }else{
            txQTitle.setText("");
        }
        //iconLabel.setText("\u2302");
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (qd.getType().equalsIgnoreCase("Delivery")){
                     Intent in = new Intent(context, DeliveryActivity.class);
                     context.startActivity(in);
                }else if(qd.getType().equalsIgnoreCase("Sales")){
                    Intent in = new Intent(context, SalesActivity.class);
                    context.startActivity(in);
                }else if(qd.getType().equalsIgnoreCase("Sales Call")){
                    Intent in = new Intent(context, SalesCallActivity.class);
                    context.startActivity(in);
                }else if(qd.getType().equalsIgnoreCase("Messages")){
                     Intent in = new Intent(context, MessageActivity.class);
                     context.startActivity(in);
                }else if(qd.getType().equalsIgnoreCase("Map")){
                    Intent in = new Intent(context, MapActivity.class);
                    context.startActivity(in);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return listQuestion.size();
    }

}
