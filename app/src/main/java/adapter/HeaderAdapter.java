package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rsf.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import model.DDObject;


public class HeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<DDObject> list = new ArrayList<DDObject>();
    private Context context;

   // String[] data;

    public HeaderAdapter(List<DDObject> list,Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
            return new VHItem(viewItem);
        } else if (viewType == TYPE_HEADER) {
            View viewHead = LayoutInflater.from(parent.getContext()).inflate(R.layout.head_row, parent, false);
            return new VHHeader(viewHead);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            final DDObject dataItem = getItem(position);
            //cast holder to VHItem and set data
             final HashSet<String> hashSet = new HashSet<String>();

            ((VHItem) holder).tvProduct.setText(dataItem.getDetail().getP_name());
            ((VHItem) holder).tvQuantity.setText(dataItem.getDetail().getP_order_quantity());
            ((VHItem) holder).tvDeliveryQuantity.setText(dataItem.getDetail().getP_delivery_quantity());
            ((VHItem) holder).tvSerial.setText(dataItem.getDetail().getP_serial_quantity());

            if (dataItem.getDetail().getIs_serializable().equalsIgnoreCase("1")){

            }else{
                ((VHItem) holder).tvSerial.setEnabled(false);
                ((VHItem) holder).llSerialValue.setVisibility(View.INVISIBLE);
            }

            ((VHItem) holder).addSerialBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.row, null);
                    TextView textOut = (TextView)addView.findViewById(R.id.textout);
                    final String serialNumber = ((VHItem) holder).tvSerial.getText().toString().trim();

                    if (!TextUtils.isEmpty(serialNumber) && (!hashSet.contains(serialNumber))){
                        hashSet.add(serialNumber);
                        textOut.setText(serialNumber);
                        ((VHItem) holder).tvSerial.setText("");
                        ((VHItem) holder).addViewSerial.addView(addView);

                        for (String s: hashSet) {
                            Log.e(">>>",""+s);
                        }

                    }else{
                        Toast.makeText(context,"Already added or empty field",Toast.LENGTH_SHORT).show();
                    }

                    ImageView buttonRemove = (ImageView) addView.findViewById(R.id.remove);
                    buttonRemove.setOnClickListener(new View.OnClickListener(){

                        @Override
                        public void onClick(View v) {
                            hashSet.remove(serialNumber);
                            ((LinearLayout)addView.getParent()).removeView(addView);

                            for (String s: hashSet) {
                                Log.e(">>>",""+s);
                            }

                        }});
                }
            });


        } else if (holder instanceof VHHeader) {
            //cast holder to VHHeader and set data for header.
        }

    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

            return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }
   private DDObject getItem(int position) {
       // return data[position - 1];
       return list.get(position-1);
    }

    class VHItem extends RecyclerView.ViewHolder {
        protected TextView tvProduct,tvQuantity;
        private EditText tvDeliveryQuantity,tvSerial;
        private LinearLayout addViewSerial;
        private RelativeLayout llSerialValue;
        protected ImageView addSerialBtn;
        protected View mRootView;

        public VHItem(View itemView) {
            super(itemView);
            tvProduct = (TextView) itemView.findViewById(R.id.tvProduct);
            tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            tvDeliveryQuantity = (EditText) itemView.findViewById(R.id.tvDeliveryQuantity);
            tvSerial = (EditText) itemView.findViewById(R.id.tvSerial);
            addSerialBtn = (ImageView) itemView.findViewById(R.id.addSerialBtn);
            addViewSerial = (LinearLayout) itemView.findViewById(R.id.addViewSerial);
            llSerialValue = (RelativeLayout) itemView.findViewById(R.id.llSerialValue);

            mRootView = itemView;
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
       protected TextView tvProductTitle,tvQuantityTitle,tvDeliveryTitle,tvSerialTitle;
        public VHHeader(View itemView) {
            super(itemView);
            tvProductTitle = (TextView) itemView.findViewById(R.id.tvProductTitle);
            tvQuantityTitle = (TextView) itemView.findViewById(R.id.tvQuantityTitle);
            tvDeliveryTitle = (TextView) itemView.findViewById(R.id.tvDeliveryTitle);
            tvSerialTitle = (TextView) itemView.findViewById(R.id.tvSerialTitle);


        }
    }
}
