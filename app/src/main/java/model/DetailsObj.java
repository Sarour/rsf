package model;

/**
 * Created by Salahuddin on 1/12/2017.
 */

public class DetailsObj {
    private String type;
    private Qdetails qdetail = new Qdetails();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Qdetails getQdetail() {
        return qdetail;
    }

    public void setQdetail(Qdetails qdetail) {
        this.qdetail = qdetail;
    }
}
