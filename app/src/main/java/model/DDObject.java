package model;

import java.util.ArrayList;
import java.util.List;

public class DDObject{

    private String type;
    private DDetails detail= new DDetails();
    private List<String> serials = new ArrayList<String>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DDetails getDetail() {
        return detail;
    }

    public void setDetail(DDetails detail) {
        this.detail = detail;
    }

    public List<String> getSerials() {
        return serials;
    }

    public void setSerials(List<String> serials) {
        this.serials = serials;
    }
}
