package model;

/**
 * Created by Salahuddin on 3/13/2017.
 */

public class AlertObj {
    private String AlertType;
    //private String DefaultDeviceUserID;
    private String Frequency;
   // private String MessageCode;
   // private String MessageText;
    private String NotificationText;
   // private String NotificationURL;
   // private String ServiceAccessCode;
    private String Status;
    private String Time;

    public String getAlertType() {
        return AlertType;
    }

    public void setAlertType(String alertType) {
        AlertType = alertType;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public String getNotificationText() {
        return NotificationText;
    }

    public void setNotificationText(String notificationText) {
        NotificationText = notificationText;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

}
