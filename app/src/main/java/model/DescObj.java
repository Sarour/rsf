package model;

/**
 * Created by Salahuddin on 3/16/2017.
 */

public class DescObj {
    private String type;
    private String address;
    private String mobile;
    private String total_customer;
    private String retail_type;
    private String lat;
    private String lng;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTotal_customer() {
        return total_customer;
    }

    public void setTotal_customer(String total_customer) {
        this.total_customer = total_customer;
    }

    public String getRetail_type() {
        return retail_type;
    }

    public void setRetail_type(String retail_type) {
        this.retail_type = retail_type;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
