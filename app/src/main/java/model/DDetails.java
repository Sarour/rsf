package model;
public class DDetails {
    private String p_name;
    private String p_order_quantity;
    private String p_delivery_quantity;
    private String p_serial_quantity;
    private String is_serializable;

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_order_quantity() {
        return p_order_quantity;
    }

    public void setP_order_quantity(String p_order_quantity) {
        this.p_order_quantity = p_order_quantity;
    }

    public String getP_delivery_quantity() {
        return p_delivery_quantity;
    }

    public void setP_delivery_quantity(String p_delivery_quantity) {
        this.p_delivery_quantity = p_delivery_quantity;
    }

    public String getP_serial_quantity() {
        return p_serial_quantity;
    }

    public void setP_serial_quantity(String p_serial_quantity) {
        this.p_serial_quantity = p_serial_quantity;
    }

    public String getIs_serializable() {
        return is_serializable;
    }

    public void setIs_serializable(String is_serializable) {
        this.is_serializable = is_serializable;
    }
}
